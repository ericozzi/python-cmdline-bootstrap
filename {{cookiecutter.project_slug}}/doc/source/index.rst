Welcome to {{ cookiecutter.project_name }}'s documentation!
====================================

This is a Python project template. You can follow the instructions in ``README.md`` to
create your own Python project from this repository.

This text is located in the ``{{ cookiecutter.project_slug }}/doc/source/index.rst`` file. You should update
it to match your new project. The files located in ``{{ cookiecutter.project_slug }}/doc/source/api`` are
automatically generated each time that `sphinxdoc` is run, and should not be edited
by hand, as any changes will be lost.


This is an example of the autogenerated documentation.

.. toctree::

    api/autoindex


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
