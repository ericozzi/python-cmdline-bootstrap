:orphan:

{{cookiecutter.project_name}} manual page
=================

synopsis
--------

**{{cookiecutter.project_slug}}** [--version | -V] [--help | -h] [--verbose | -v] [--quiet | -q] <command> [<args>]

description
-----------

{{cookiecutter.project_short_description}}

options
-------

-V, --version
    Prints the version of the image command.

-h, --help
    Prints the synopsis and a list of available commands and options.
    When --help is combined with a valid sub-command, specific usage
    options for the sub-command will be displayed.

-v, --verbose
    Increases the level of verbosity of command output.

-q, --quiet
    Suppresses all non-essential command output.

{{cookiecutter.project_slug}} sub-commands
------------------

**{{cookiecutter.project_slug}}** cmd1
    Command description

cmd1
----

SYNOPSIS
^^^^^^^^

**image** cmd1 [--opt1]

DESCRIPTION
^^^^^^^^^^^


OPTIONS
^^^^^^^

--opt1 
    Decription of option 1

EXAMPLES
^^^^^^^^

**{{cookiecutter.project_slug}} -v cmd1 **
    Description
