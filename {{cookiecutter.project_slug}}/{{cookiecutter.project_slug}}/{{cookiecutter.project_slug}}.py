# -*- coding: utf-8 -*-


"""{{cookiecutter.project_slug}}.{{cookiecutter.project_slug}}: provides entry point main()."""


__version__ = "{{cookiecutter.version}}"

import logging
from .stuff import Stuff
from .{{cookiecutter.project_slug}}_parser import create_{{cookiecutter.project_slug}}_parser

logger = logging.getLogger()
stdout_handler = logging.StreamHandler()
stdout_formatter = logging.Formatter('%(name)s - %(module)s - %(message)s')
stdout_handler.setFormatter(stdout_formatter)
logger.addHandler(stdout_handler)
logger.setLevel(logging.INFO)

class BootstrapApp:
    def cmd1(self, args):
        Stuff()

    def cmd2(self, args):
        pass

def main(args):
    # Build command parser and parse user supplied arguments
    parser = create_{{cookiecutter.project_slug}}_parser("{{cookiecutter.project_slug}}")
    args = parser.parse_args(args)

    bs = BootstrapApp()

    logger.info("Executing {{cookiecutter.project_slug}} version %s." % __version__)
    logger.debug("cli args: %s" % (args,))

    # Copy the contents of the Namespace into a map and remove unneeded args
    argsmap = dict(vars(args))
    del argsmap['command']
    del argsmap['verbose']
    del argsmap['quiet']

    logger.debug("cli argsmap: %s" % (argsmap,))

    getattr(bs, args.command)(args)


