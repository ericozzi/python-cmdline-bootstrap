import argparse
import argcomplete
import logging

logger = logging.getLogger()

class QuietAction(argparse.Action):
    def __call__(self, parser, *args, **kwargs):
        """
        Executed from within the parser as an action when
        '-q|--quiet' is encountered. Sets the level of the
        logger to only print messages of ERROR or
        CRITICAL levels.
        """
        logger.setLevel(logging.ERROR)

class VerboseAction(argparse.Action):
    count = 0

    def __call__(self, parser, *args, **kwargs):
        """
        Executed from within the parser as an action when
        '-v|--verbose' is encountered. Sets the level of the
        logger to DEBUG.
        """
        self.count += 1
        if self.count >= 1:
            logger.setLevel(logging.DEBUG)

            # This fragile hack sets the API's stdout handler to
            # logging.DEBUG level.  Really, the CLI should own
            # the stdout handler.
            if len(logger.handlers) > 1:
                logger.handlers[0].setLevel(logging.DEBUG)

def cmd1_completer(prefix, parsed_args, **kwargs):
    """
    This method is called during command line construction
    through bash tab-completion hooks. The parser calls
    various completers based on their validity and aggregates
    the results into a tab-completion list. This method
    returns a list of valid repositories.
    """
    return ["foo", "bar", "baz"]

def cmd2_completer(prefix, parsed_args, **kwargs):
    """
    This method is called during command line construction
    through bash tab-completion hooks. The parser calls
    various completers based on their validity and aggregates
    the results into a tab-completion list. This method
    returns a list of valid repositories.
    """
    return ["foo", "bar", "baz"]

def add_cmd1_subparser(subparsers, parent_parser):
    parser = subparsers.add_parser('cmd1', parents=[parent_parser],
            formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('-f', '--force',
            action='store_true',
            help='force')
    parser.add_argument('command1',
            metavar='command1',
            type=str,
            nargs='*',
            help='name').completer = cmd1_completer

def add_cmd2_subparser(subparsers, parent_parser):
    parser = subparsers.add_parser('cmd2', parents=[parent_parser],
            formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('command2',
            metavar='command2',
            type=str,
            nargs='*',
            help='name').completer = cmd2_completer

def create_{{cookiecutter.project_slug}}_parser(program_name):
    parent_parser = argparse.ArgumentParser(prog=program_name, add_help=False)

    parent_parser.add_argument('-v', '--verbose',
            action=VerboseAction,
            nargs=0,
            help='increase the verbosity of the command')
    parent_parser.add_argument('-q', '--quiet',
            action=QuietAction,
            nargs=0,
            help='suppresses unnecessary output')

    parser = argparse.ArgumentParser(parents=[parent_parser],
               formatter_class=argparse.RawDescriptionHelpFormatter)

    subparsers = parser.add_subparsers(title='subcommands', dest='command')
    add_cmd1_subparser(subparsers, parent_parser)
    add_cmd2_subparser(subparsers, parent_parser)
    argcomplete.autocomplete(parser)

    return parser
