# -*- coding: utf-8 -*-

import logging

"""{{cookiecutter.project_slug}}.stuff: stuff module within the {{cookiecutter.project_slug}} package."""

LOGGER = logging.getLogger('{{cookiecutter.project_slug}}.{{cookiecutter.project_slug}}')

def Stuff():
    LOGGER.critical("critical")
    LOGGER.error("error")
    LOGGER.warning("warn")
    LOGGER.info("info")
    LOGGER.debug("debug")
